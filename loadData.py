import os
import pandas as pd
import nltk
#nltk.download('popular')
from nltk.tokenize import word_tokenize
from elasticsearch import Elasticsearch
es = Elasticsearch()

from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score

folder1 = 'train_with'
folder2 = 'train_none'
df = pd.DataFrame()
num_of_training_set = 0

for file in os.listdir(folder1):
    f = open("train_with/%s" % file, "r")
    content = f.read().lower()
    #print(content)
    num_of_training_set += 1
    df = df.append([[content, '1']], ignore_index=True)

for file in os.listdir(folder2):
    f = open("train_none/%s" % file, "r")
    content = f.read().lower()
    #print(content)
    num_of_training_set += 1
    df = df.append([[content, '0']], ignore_index=True)

df.columns = ['comment', 'hate']
print(df)
df.to_csv('comments.csv', index=False, encoding='utf-8')

comments = df.comment.str.cat(sep=' ')
tokens = word_tokenize(comments)
vocabulary = set(tokens)

frequency_dist = nltk.FreqDist(tokens)
s = sorted(frequency_dist, key=frequency_dist.__getitem__, reverse=True)
s = [w for w in s if len(w) > 2]

print(s)


X_train = df.loc[0:num_of_training_set, 'comment'].values
y_train = df.loc[0:num_of_training_set, 'hate'].values
#print(X_train)

file_to_test = open("test/test.txt", "r")
comment_to_test = file_to_test.read().lower() 
X_test = [comment_to_test]

#print(comment_to_test)
print()
print('==== vectorization and conclusion ====')
print()

vectorizer = TfidfVectorizer()
train_vectors = vectorizer.fit_transform(X_train)
test_vectors= vectorizer.transform(X_test)
#print(train_vectors.shape, test_vectors.shape)

clf = MultinomialNB().fit(train_vectors, y_train)
predicted = clf.predict(test_vectors)
print(predicted)
